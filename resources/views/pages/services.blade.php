@extends('layouts.app_old') 


@section('content')
    <h1>{{$title}}</h1>
    <p>This is the services section</p>
    @if(count($services) > 0 )
    @foreach($services as $service)
       <ul>
            <li> {{$service}} </li>
        </ul>
    @endforeach
    @else 
    <h3>Nothing to display</h3>
@endif
@endsection